// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Mult.asm

// Multiplies R0 and R1 and stores the result in R2.
// (R0, R1, R2 refer to RAM[0], RAM[1], and RAM[2], respectively.)

// Put your code here.
@R1
D=M
@E
M=D
@R0
D=M
@R2
M=0
@B
M=D
@K
M=1
(JMP1)
@E
D=M
@F
M=D
@F
D=M
@K
D=M
@B
D=D&M
@R
M=D
@K
D=M
M=M+D
@E
D=M
MD=D+M
@R
D=M
@JMP1
D;JEQ
@F
D=M
@R2
M=M+D
@K
D=M
@JMP2
D;JEQ
@JMP1
0;JMP
(JMP2)
@R2
D=M
@R2
M=D