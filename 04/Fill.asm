// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input.
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel;
// the screen should remain fully black as long as the key is pressed. 
// When no key is pressed, the program clears the screen, i.e. writes
// "white" in every pixel;
// the screen should remain fully clear as long as no key is pressed.

// Put your code here.
@KEY
D=M
(LOOP)
@JMP1
D;JEQ
(JMP2)
@SCREEN
D=A
@AA
M=D
@AA
M=1
D=A
@AA
M=D
@AA
M=M+1
@65535
D=A
@AA
D=D-M
@JMP2
D;JNE
@END
0;JMP
(JMP1)
(JMP2)
@SCREEN
D=A
@AA
M=D
@AA
M=0
D=A
@AA
M=D
@AA
M=M+1
@65535
D=A
@AA
D=D-M
@JMP2
D;JNE
@END
(END)
@LOOP
0;JMP