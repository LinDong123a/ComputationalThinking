// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/DMux8Way.hdl

/**
 * 8-way demultiplexor:
 * {a, b, c, d, e, f, g, h} = {in, 0, 0, 0, 0, 0, 0, 0} if sel == 000
 *                            {0, in, 0, 0, 0, 0, 0, 0} if sel == 001
 *                            etc.
 *                            {0, 0, 0, 0, 0, 0, 0, in} if sel == 111
 */

CHIP DMux8Way {
    IN in, sel[3];
    OUT a, b, c, d, e, f, g, h;

    PARTS:
    Not(in=sel[0],out=p);
    Not(in=sel[1],out=q);
    Not(in=sel[2],out=r);
    And(a=p,b=q,out=va);
    And(a=va,b=r,out=wa);
    And(a=wa,b=in,out=a);
    And(a=sel[0],b=q,out=vb);
    And(a=vb,b=r,out=wb);
    And(a=wb,b=in,out=b);
    And(a=p,b=sel[1],out=vc);
    And(a=vc,b=r,out=wc);
    And(a=wc,b=in,out=c);
    And(a=sel[0],b=sel[1],out=vd);
    And(a=vd,b=r,out=wd);
    And(a=wd,b=in,out=d);
    And(a=p,b=q,out=ve);
    And(a=ve,b=sel[2],out=we);
    And(a=we,b=in,out=e);
    And(a=sel[0],b=q,out=vf);
    And(a=vf,b=sel[2],out=wf);
    And(a=wf,b=in,out=f);
    And(a=p,b=sel[1],out=vg);
    And(a=vg,b=sel[2],out=wg);
    And(a=wg,b=in,out=g);
    And(a=sel[0],b=sel[1],out=vh);
    And(a=vh,b=sel[2],out=wh);
    And(a=wh,b=in,out=h);
}