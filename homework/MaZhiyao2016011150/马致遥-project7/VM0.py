# -*- coding: UTF-8 -*-
name=raw_input("Type the file name:")
j=name.rfind(".")
name=name[0:j]
#去除注释
vm0=open(name+".vm","rb")
vm1=open("NE1.vm","wb")
vm0.seek(0,2)
end=vm0.tell()
vm0.seek(0,0)
while (vm0.tell()<end-1):
    dbit=vm0.read(2)
    if dbit=="//":
        bit=vm0.read(1)
        while (bit!="\n"):
            bit=vm0.read(1)
        vm1.write("\n")
    else:
        vm0.seek(-2,1)
        bit=vm0.read(1)
        vm1.write(bit)
vm1.write(vm0.read(1))
vm0.close()
vm1.close()
#去除行两端空格
vm0=open("NE1.vm","rb")
vm1=open("NE2.vm","wb")
vm0.seek(0,2)
end=vm0.tell()
vm0.seek(0,0)
while (vm0.tell()<end):
    line=vm0.readline()
    i=0
    line=" "+line
    while line[i]==" ":
        i=i+1
    j=len(line)-1
    while line[j]==" ":
        j=j-1
    j=j+1
    line=line[i:j]
    vm1.write(line)
vm0.close()
vm1.close()
#去除空行
vm0=open("NE1.vm","rb")
vm1=open("NE.vm","wb")
vm0.seek(0,2)
end=vm0.tell()
vm0.seek(0,0)
while (vm0.tell()<end):
    line=vm0.readline()
    if (line!="\n" and line!="\r\n"):
        vm1.write(line)
vm0.close()
vm1.close()
#翻译
vm0=open("NE.vm","r")
vm1=open(name+".asm","w")
vm0.seek(0,2)
end=vm0.tell()
vm0.seek(0,0)
symbol_ALC=0
while (vm0.tell()<end):
    s=str(symbol_ALC)
    line=vm0.readline()
    if line=="add\n":
        vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nM=D+M\n")
    elif line=="sub\n":
        vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nM=M-D\n")
    elif line=="eq\n":
        vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nD=M-D\n@Else"+s+"\nD;JEQ\n@SP\nA=M-1\nM=0\n@End"+s+"\n0;JMP\n(Else"+s+")\n@SP\nA=M-1\nM=-1\n(End"+s+")\n")
        symbol_ALC=symbol_ALC+1
    elif line=="gt\n":
        vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nD=M-D\n@Else"+s+"\nD;JGT\n@SP\nA=M-1\nM=0\n@End"+s+"\n0;JMP\n(Else"+s+")\n@SP\nA=M-1\nM=-1\n(End"+s+")\n")
        symbol_ALC=symbol_ALC+1
    elif line=="lt\n":
        vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nD=M-D\n@Else"+s+"\nD;JLT\n@SP\nA=M-1\nM=0\n@End"+s+"\n0;JMP\n(Else"+s+")\n@SP\nA=M-1\nM=-1\n(End"+s+")\n")
        symbol_ALC=symbol_ALC+1
    elif line=="and\n":
        vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nM=D&M\n")
        symbol_ALC=symbol_ALC+1
    elif line=="or\n":
        vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nM=D|M\n")
        symbol_ALC=symbol_ALC+1
    elif line=="neg\n":
        vm1.write("@SP\nA=M-1\nM=-M\n")
    elif line=="not\n":
        vm1.write("@SP\nA=M-1\nM=!M\n")
    elif ("push" in line or "pop" in line):
        i=line.rfind(" ")+1
        n=line[i:len(line)-1]
        if "constant" in line:
            vm1.write("@"+n+"\nD=A\n@SP\nM=M+1\nA=M-1\nM=D\n")
        elif "static" in line:
            if "push" in line:
                vm1.write("@"+name+"."+n+"\nD=M\n@SP\nM=M+1\nA=M-1\nM=D\n")
            if "pop" in line:
                vm1.write("@SP\nAM=M-1\nD=M\nM=0\n@"+name+"."+n+"\nM=D\n")
        else:
            if "push" in line:
                pp="A=D+A\nD=M\n@SP\nM=M+1\nA=M-1\nM=D\n"
            if "pop" in line:
                pp="D=D+A\n@R13\nM=D\n@SP\nAM=M-1\nD=M\nM=0\n@R13\nA=M\nM=D\n"
            if "local" in line:
                ad="@LCL\nD=M\n"
            if "argument" in line:
                ad="@ARG\nD=M\n"
            if "this" in line:
                ad="@THIS\nD=M\n"
            if "that" in line:
                ad="@THAT\nD=M\n"
            if "pointer" in line:
                ad="@THIS\nD=A\n"
            if "temp" in line:
                ad="@R5\nD=A\n"
            vm1.write(ad+"@"+n+"\n"+pp)
    print "line finished"
print "Done"
vm0.close()
vm1.close()
import os
os.remove("NE.vm")
os.remove("NE1.vm")
os.remove("NE2.vm")
