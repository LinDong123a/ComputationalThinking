# -*- coding: UTF-8 -*-
#函数：判断一个字符是否为数字
def ifnumber(x):
    if x=="0" or x=="1" or x=="2" or x=="3" or x=="4" or x=="5" or x=="6" or x=="7" or x=="8" or x=="9":
        return 1
    else:
        return 0
#函数：判断一个元素在列表里第几个
def numberinlist(x,thelist):
    i=0
    while i<len(thelist):
        if x==thelist[i]:
            return i
        i=i+1
    return "null"
#函数：将一个正整数转化为15位二进制数,以字符形式输出
def binary15(x):
    if x>32767:
        return "overflow"
    else:
        i=14
        out=""
        while i>=0:
            out=out+str(x//(2**i))
            x=x%(2**i)
            i=i-1
        return out
#去除注释
asm0=open("Pong.asm","rb")
asm1=open("NE1.asm","wb")
asm0.seek(0,2)
end=asm0.tell()
asm0.seek(0,0)
while (asm0.tell()<end-1):
    dbit=asm0.read(2)
    if dbit=="//":
        bit=asm0.read(1)
        while (bit!="\n"):
            bit=asm0.read(1)
        asm1.write("\n")
    else:
        asm0.seek(-2,1)
        bit=asm0.read(1)
        if bit!=" ":
            asm1.write(bit)
asm1.write(asm0.read(1))
asm0.close()
asm1.close()
#去除空行
asm0=open("NE1.asm","r")
asm1=open("NE.asm","w")
asm0.seek(0,2)
end=asm0.tell()
asm0.seek(0,0)
while (asm0.tell()<end):
    line=asm0.readline()
    if line!="\n":
        asm1.write(line)
asm0.close()
asm1.close()
#生成变量表
asm0=open("NE.asm","r")
ln=0
mn=16
varlist=["SP","LCL","ARG","THIS","THAT","R0","R1","R2","R3","R4","R5","R6","R7","R8","R9","R10","R11","R12","R13","R14","R15","SCREEN","KBD"]
valuelist=[0,1,2,3,4,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16384,24576]
asm0.seek(0,2)
end=asm0.tell()
asm0.seek(0,0)
while (asm0.tell()<end):
    ln=ln+1
    line=asm0.readline()
    if line[0]=="(" and ifnumber(x=line[1])==0:
        i=1
        var=""
        while line[i]!=")":
            var=var+line[i]
            i=i+1
        if numberinlist(x=var,thelist=varlist)=="null":
            varlist.append(var)
            valuelist.append(ln-1)
            ln=ln-1
asm0.seek(0,0)
while (asm0.tell()<end):
    line=asm0.readline()
    if line[0]=="@" and ifnumber(x=line[1])==0:
        i=1
        var=""
        while line[i]!="\n":
            var=var+line[i]
            i=i+1
        if numberinlist(x=var,thelist=varlist)=="null":
            varlist.append(var)
            valuelist.append(mn)
            mn=mn+1
asm0.close()
#翻译
asm0=open("NE.asm","r")
asm1=open("Out.hack","w")
asm0.seek(0,2)
end=asm0.tell()
asm0.seek(0,0)
while (asm0.tell()<end):
    line=asm0.readline()
    if line[0]=="@":
        i=1
        var=""
        while line[i]!="\n":
            var=var+line[i]
            i=i+1
        if ifnumber(x=line[1])==0:
            j=numberinlist(x=var,thelist=varlist)
            var=str(valuelist[j])
        instruction="0"+binary15(int(var))+"\n"
        asm1.write(instruction)    
    elif "(" not in line:
        il=[1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0]
        j=0
        if "=" in line:
            i=0
            dest=""
            while line[i]!="=":
                dest=dest+line[i]
                i=i+1
            j=i+1
            if "A" in dest:
                il[10]=1
            if "D" in dest:
                il[11]=1
            if "M" in dest:
                il[12]=1
        if ";" in line:
            i=0
            jump=""
            while line[i]!=";":
                i=i+1
            i=i+1
            while line[i]!="\n":
                jump=jump+line[i]
                i=i+1
            if jump=="JGT":
                il[13],il[14],il[15]=0,0,1
            if jump=="JEQ":
                il[13],il[14],il[15]=0,1,0
            if jump=="JGE":
                il[13],il[14],il[15]=0,1,1
            if jump=="JLT":
                il[13],il[14],il[15]=1,0,0
            if jump=="JNE":
                il[13],il[14],il[15]=1,0,1
            if jump=="JLE":
                il[13],il[14],il[15]=1,1,0
            if jump=="JMP":
                il[13],il[14],il[15]=1,1,1
        i=j
        comp=""
        while (line[i]!="\n" and line[i]!=";"):
            comp=comp+line[i]
            i=i+1
        if comp=="0":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,1,0,1,0,1,0
        if comp=="1":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,1,1,1,1,1,1
        if comp=="-1":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,1,1,1,0,1,0
        if comp=="D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,0,1,1,0,0
        if comp=="A":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,1,1,0,0,0,0
        if comp=="!D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,0,1,1,0,1
        if comp=="!A":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,1,1,0,0,0,1
        if comp=="-D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,0,1,1,1,1
        if comp=="-A":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,1,1,0,0,1,1
        if comp=="D+1":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,1,1,1,1,1
        if comp=="A+1":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,1,1,0,1,1,1
        if comp=="D-1":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,0,1,1,1,0
        if comp=="A-1":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,1,1,0,0,1,0
        if comp=="D+A" or comp=="A+D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,0,0,0,1,0
        if comp=="D-A":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,1,0,0,1,1
        if comp=="A-D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,0,0,1,1,1
        if comp=="D&A" or comp=="A&D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,0,0,0,0,0
        if comp=="D|A" or comp=="A|D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=0,0,1,0,1,0,1

        if comp=="M":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,1,1,0,0,0,0
        if comp=="!M":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,1,1,0,0,0,1
        if comp=="-M":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,1,1,0,0,1,1
        if comp=="M+1":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,1,1,0,1,1,1
        if comp=="M-1":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,1,1,0,0,1,0
        if comp=="D+M" or comp=="M+D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,0,0,0,0,1,0
        if comp=="D-M":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,0,1,0,0,1,1
        if comp=="M-D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,0,0,0,1,1,1
        if comp=="D&M" or comp=="M&D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,0,0,0,0,0,0
        if comp=="D|M" or comp=="M|D":
            il[3],il[4],il[5],il[6],il[7],il[8],il[9]=1,0,1,0,1,0,1
        instruction=""
        i=0
        while i<16:
            instruction=instruction+str(il[i])
            i=i+1
        instruction=instruction+"\n"
        asm1.write(instruction)
asm0.close()
asm1.close()
import os
os.remove("NE.asm")
os.remove("NE1.asm")
print "Done"
