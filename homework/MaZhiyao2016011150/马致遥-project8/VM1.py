# -*- coding: UTF-8 -*-
#翻译一个文件
def translate(vmname,name):
    global symbol_R,symbol_ALC
    #去除注释
    vm0=open(vmname+".vm","rb")
    vm1=open(vmname+"NE1.vmx","wb")
    vm0.seek(0,2)
    end=vm0.tell()
    vm0.seek(0,0)
    while (vm0.tell()<end-1):
        dbit=vm0.read(2)
        if dbit=="//":
            bit=vm0.read(1)
            while (bit!="\n"):
                bit=vm0.read(1)
            vm1.write("\r\n")
        else:
            vm0.seek(-2,1)
            bit=vm0.read(1)
            vm1.write(bit)
    vm1.write(vm0.read(1))
    vm0.close()
    vm1.close()
    #去除行两端空格
    vm0=open(vmname+"NE1.vmx","rb")
    vm1=open(vmname+"NE2.vmx","wb")
    vm0.seek(0,2)
    end=vm0.tell()
    vm0.seek(0,0)
    while (vm0.tell()<end):
        line=vm0.readline()
        i=0
        line=" "+line
        while line[i]==" ":
            i=i+1
        j=len(line)-3
        while line[j]==" ":
            j=j-1
        j=j+1
        line=line[i:j]+"\r\n"
        vm1.write(line)
    vm0.close()
    vm1.close()
    #去除空行
    vm0=open(vmname+"NE2.vmx","rb")
    vm1=open(vmname+"NE.vmx","wb")
    vm0.seek(0,2)
    end=vm0.tell()
    vm0.seek(0,0)
    while (vm0.tell()<end):
        line=vm0.readline()
        if (line!="\n" and line!="\r\n"):
            vm1.write(line)
    vm1.write("\r\n")
    vm0.close()
    vm1.close()
    #翻译
    vm0=open(vmname+"NE.vmx","r")
    vm1=open(name+".asm","a")
    vm0.seek(0,2)
    end=vm0.tell()
    vm0.seek(0,0)
    fname=""
    while (vm0.tell()<end):
        r=str(symbol_R)
        s=str(symbol_ALC)
        line=vm0.readline()
        i=line.find(" ")
        linef=line[0:i]#第一个空格（如果有的话）之前的部分
        i=line.rfind(" ")+1
        n=line[i:len(line)-1]#最后一个空格（如果有的话）之后直到换行符的部分
        i=line.find(" ")+1
        j=line.rfind(" ")
        f=line[i:j]#如果有两个空格（对于本VM语言不可能更多）则其之间的部分
        if linef=="label":
            if fname=="":
                vm1.write("(Label_"+n+")\n")
            else:
                vm1.write("("+fname+"_Label_"+n+")\n")
        elif linef=="goto":
            if fname=="":
                vm1.write("@Label_"+n+"\n0;JMP\n")
            else:
                vm1.write("@"+fname+"_Label_"+n+"\n0;JMP\n")
        elif linef=="if-goto":
            if fname=="":
                vm1.write("@SP\nAM=M-1\nD=M\n@Label_"+n+"\nD;JNE\n")
            else:
                vm1.write("@SP\nAM=M-1\nD=M\n@"+fname+"_Label_"+n+"\nD;JNE\n")
        elif linef=="call":
            vm1.write("@Return"+r+"\n")
            vm1.write('''D=A
@SP
M=M+1
A=M-1
M=D
@LCL
D=M
@SP
M=M+1
A=M-1
M=D
@ARG
D=M
@SP
M=M+1
A=M-1
M=D
@THIS
D=M
@SP
M=M+1
A=M-1
M=D
@THAT
D=M
@SP
M=M+1
A=M-1
M=D
@SP
D=M
@LCL
M=D
@'''+str(int(n)+5)+'''
D=D-A
@ARG
M=D
''')
            vm1.write("@Function_"+f+"\n0;JMP\n(Return"+r+")\n")
            symbol_R=symbol_R+1
        elif linef=="function":
            fname=f
            vm1.write("(Function_"+f+")\n")
            k=1
            while(k<=int(n)):
                vm1.write("@SP\nM=M+1\nA=M-1\nM=0\n")
                k=k+1
        elif line=="return\n":
            vm1.write('''@LCL
D=M
@5
A=D-A
D=M
@R14
M=D
@SP
AM=M-1
D=M
@ARG
A=M
M=D
@ARG
D=M
@SP
M=D+1
@LCL
D=M
@1
A=D-A
D=M
@THAT
M=D
@LCL
D=M
@2
A=D-A
D=M
@THIS
M=D
@LCL
D=M
@3
A=D-A
D=M
@ARG
M=D
@LCL
D=M
@4
A=D-A
D=M
@LCL
M=D
@R14
A=M
0;JMP
''')
        elif line=="add\n":
            vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nM=D+M\n")
        elif line=="sub\n":
            vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nM=M-D\n")
        elif line=="eq\n":
            vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nD=M-D\n@Else"+s+"\nD;JEQ\n@SP\nA=M-1\nM=0\n@End"+s+"\n0;JMP\n(Else"+s+")\n@SP\nA=M-1\nM=-1\n(End"+s+")\n")
            symbol_ALC=symbol_ALC+1
        elif line=="gt\n":
            vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nD=M-D\n@Else"+s+"\nD;JGT\n@SP\nA=M-1\nM=0\n@End"+s+"\n0;JMP\n(Else"+s+")\n@SP\nA=M-1\nM=-1\n(End"+s+")\n")
            symbol_ALC=symbol_ALC+1
        elif line=="lt\n":
            vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nD=M-D\n@Else"+s+"\nD;JLT\n@SP\nA=M-1\nM=0\n@End"+s+"\n0;JMP\n(Else"+s+")\n@SP\nA=M-1\nM=-1\n(End"+s+")\n")
            symbol_ALC=symbol_ALC+1
        elif line=="and\n":
            vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nM=D&M\n")
            symbol_ALC=symbol_ALC+1
        elif line=="or\n":
            vm1.write("@SP\nAM=M-1\nD=M\nM=0\nA=A-1\nM=D|M\n")
            symbol_ALC=symbol_ALC+1
        elif line=="neg\n":
            vm1.write("@SP\nA=M-1\nM=-M\n")
        elif line=="not\n":
            vm1.write("@SP\nA=M-1\nM=!M\n")
        elif ("push"==linef or "pop"==linef):
            if "constant" in line:
                vm1.write("@"+n+"\nD=A\n@SP\nM=M+1\nA=M-1\nM=D\n")
            elif "static" in line:
                if "push" in line:
                    vm1.write("@"+vmname+"."+n+"\nD=M\n@SP\nM=M+1\nA=M-1\nM=D\n")
                if "pop" in line:
                    vm1.write("@SP\nAM=M-1\nD=M\nM=0\n@"+vmname+"."+n+"\nM=D\n")
            else:
                if "push" in line:
                    pp="A=D+A\nD=M\n@SP\nM=M+1\nA=M-1\nM=D\n"
                if "pop" in line:
                    pp="D=D+A\n@R13\nM=D\n@SP\nAM=M-1\nD=M\nM=0\n@R13\nA=M\nM=D\n"
                if "local" in line:
                    ad="@LCL\nD=M\n"
                if "argument" in line:
                    ad="@ARG\nD=M\n"
                if "this" in line:
                    ad="@THIS\nD=M\n"
                if "that" in line:
                    ad="@THAT\nD=M\n"
                if "pointer" in line:
                    ad="@THIS\nD=A\n"
                if "temp" in line:
                    ad="@R5\nD=A\n"
                vm1.write(ad+"@"+n+"\n"+pp)
        print "line finished"
    print "Done"
    vm0.close()
    vm1.close()
    os.remove(vmname+"NE.vmx")
    os.remove(vmname+"NE1.vmx")
    os.remove(vmname+"NE2.vmx")
#给定目录，复数.vm文件读入并翻译
import os
top=raw_input("Input the project root:")
i=top.rfind("\\")+1
name=top[i:len(top)]
os.chdir(top)
flag=0
symbol_ALC=0
symbol_R=0
asm=open(name+".asm","w")
for root,dirs,files in os.walk(top,topdown=False):
    for vmname in files:
        if vmname=="Sys.vm":flag=1
if flag:
    asm.write("@256\nD=A\n@SP\nM=D\n")
    asm.write('''@Return0
D=A
@SP
M=M+1
A=M-1
M=D
@LCL
D=M
@SP
M=M+1
A=M-1
M=D
@ARG
D=M
@SP
M=M+1
A=M-1
M=D
@THIS
D=M
@SP
M=M+1
A=M-1
M=D
@THAT
D=M
@SP
M=M+1
A=M-1
M=D
@SP
D=M
@LCL
M=D
@5
D=D-A
@ARG
M=D
@Function_Sys.init
0;JMP
(Return0)
''')
    symbol_R=symbol_R+1
asm.close()
for root,dirs,files in os.walk(top,topdown=False):
    for fullname in files:
        print fullname
        dot=fullname.rfind(".")
        fe=fullname[dot:len(fullname)]
        if fe==".vm":
            vmname=fullname[0:dot]
            translate(vmname,name)
            print vmname+".vm done"
